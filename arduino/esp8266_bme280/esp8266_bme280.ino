#include <ESP8266WiFi.h>
//#include <Wire.h> 

#include <Adafruit_BME280.h>

Adafruit_BME280 bme;

const char* ssid      = "";
const char* password  = "";

WiFiServer server(80);

float  sensor_temperature;
float  sensor_humidity;
float  sensor_pressure;
float  sensor_spare; 

String MY_tempf;
String MY_dewptf;
String MY_humidity;
String MY_baromin;

void setup() {
  Serial.begin(115200);
  boolean Status = bme.begin(0x76); 
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}


void loop() {
  WiFiClient client = server.available();
  if (client) {
    ReadSensorInformation();
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/plain;charset=utf-8");
    client.println();
    client.print("{\"tempf\":\"" + MY_tempf + "\",");
    client.print("\"dewtempf\":\"" + MY_dewptf + "\",");
    client.print("\"timestamp\":\"" + String(millis()) + "\",");
    client.print("\"baromin\":\"" + MY_baromin + "\",");
    client.print("\"humidity\":\"" + MY_humidity + "\"");
    client.print("}");
    client.println();
    delay(1);
    return;
  }
}


void ReadSensorInformation(){
  float sensor_humidity     = bme.readHumidity(); 
  float sensor_temperature  = bme.readTemperature() * 9/5+28;         // Read temperature as Fahrenheit
  float sensor_temperatureC = bme.readTemperature() - 2;
  float sensor_pressure     = bme.readPressure() / 100.0F * 0.02953 ; // Read pressure in in's"
  if (isnan(sensor_humidity) || isnan(sensor_temperature) || isnan(sensor_pressure)) { 
    Serial.println("Failed to read from sensor!"); 
    return; 
  } 
  MY_tempf     = String(sensor_temperature);
  // Td = T - ((100 - RH)/5.) where T and Td are in °C for a reasonable DP Calculation
  MY_dewptf    = String((sensor_temperature-(100-sensor_humidity)/5.0)*9/5+32); // Needs to be reported in °F
  MY_humidity  = String(sensor_humidity,2);
  MY_baromin   = String(sensor_pressure,2);
}
