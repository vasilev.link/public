#include <ESP8266WiFi.h>

#include "DHT.h"
#define DHTTYPE DHT22
uint8_t DHTPin = D2;


const char* ssid      = "";
const char* password  = "";

WiFiServer server(80);
DHT dht(DHTPin, DHTTYPE);

float  sensor_temperature;
float  sensor_humidity;
float  sensor_pressure;
float  sensor_spare; 

String MY_tempf;
String MY_dewptf;
String MY_humidity;

void setup() {
  pinMode(DHTPin, INPUT);
  dht.begin(); 
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}


void loop() {
  WiFiClient client = server.available();
  if (client) {
    ReadSensorInformation();
    client.println("HTTP/1.1 200 OK");
    client.println("Content-Type: text/plain;charset=utf-8");
    client.println();
    client.print("{\"tempf\":\"" + MY_tempf + "\",");
    client.print("\"dewtempf\":\"" + MY_dewptf + "\",");
    client.print("\"timestamp\":\"" + String(millis()) + "\",");
    client.print("\"humidity\":\"" + MY_humidity + "\"");
    client.print("}");
    client.println();
    delay(1);
    return;
  }
}


void ReadSensorInformation(){
  float sensor_humidity     = dht.readHumidity();
  float sensor_temperature  = dht.readTemperature();
  float sensor_pressure  = dht.readTemperature();
  if (isnan(sensor_humidity) || isnan(sensor_temperature) || isnan(sensor_pressure)) { 
    Serial.println("Failed to read from sensor!"); 
    return; 
  } 
  MY_tempf     = String(sensor_temperature);
  MY_dewptf    = String((sensor_temperature-(100-sensor_humidity)/5.0)*9/5+32); // Needs to be reported in °F
  MY_humidity  = String(sensor_humidity,2);
}
