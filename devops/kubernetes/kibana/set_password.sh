
#!/bin/bash

ELKPWD="changeme"
KIBPWD="changeme"

kubectl delete secret elasticsearch-pw-elastic
kubectl create secret generic elasticsearch-pw-elastic -n default --from-literal password=${ELKPWD}
kubectl delete secret kibana-pw-elastic
kubectl create secret generic kibana-pw-elastic -n default --from-literal password=${KIBPWD}