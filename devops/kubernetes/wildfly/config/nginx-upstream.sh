#!/bin/bash

if [ -n "$1" -a -n "$2" ]; then
    action="$1";
    target="$2";
    targetcp="$3";
else
    echo "Usage: $0 (add|rm) server:port server:port"
    exit 0;
fi;
CONF="/usr/share/jboss/upstream.conf"
CONFCP="/usr/share/jboss/upstreamcp.conf"
#CONF="upstream.conf"
#CONFCP="upstreamcp.conf"
SERVERS=`cat $CONF | grep server`
SERVERSCP=`cat $CONFCP | grep server`
output="upstream appsrv {
    ip_hash;"
outputcp="upstream appsrvcp {
    ip_hash;"

if [ $action == "add" ]; then
    echo -e "$output" > $CONF
    SERVERS="$SERVERS\n\tserver $target;"
    echo -e "$SERVERS" >> $CONF
    echo "}" >> $CONF

    echo -e "$outputcp" > $CONFCP
    SERVERSCP="$SERVERSCP\n\tserver $targetcp;"
    echo -e "$SERVERSCP" >> $CONFCP
    echo "}" >> $CONFCP

elif [ $action == "rm" ]; then 
    sed -i "/$target/d" $CONF
    sed -i "/$targetcp/d" $CONFCP
else 
    exit
fi