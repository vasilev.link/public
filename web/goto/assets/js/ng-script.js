var app = angular.module("clApp", ['angularUtils.directives.dirPagination']);
app.controller("clCntrl", function ($scope, $http, $location, $window) {
    $scope.apps = [];
    $scope.contentLoaded = false;
    $scope.getFILE = function () {
        angular.element(document.querySelector('#contloaded')).removeClass('hide');
        $http({
            method: 'GET',
            cache:false,
            headers:{'Cache-Control': 'no-cache'},
            data: {},
            url: './data/links.csv'
        }).then(function successCallback(response) {
            if (response != "null") {
                let lines = response.data.split('\n');
                if (lines) {
                    i = 0;
                    angular.forEach(lines, function (line, key) {
                        line = line.split(',');
                        if(key>0){
                            if (line[0]) {
                                i++;
                                $scope.apps[i] = {
                                    id: i,
                                    name: line[0] ? line[0] : "",
                                    dns: line[1] ? line[1] : "",
                                    ip: line[2] ? line[2] : "",
                                    link: line[3] ? line[3] : "",
                                    env: line[4] ? line[4] : "None",
                                    resp: line[5] ? line[5] : ""
                                };
                            }
                        }   
                    });
                }
            };
            $scope.contentLoaded = true;
        });
    };
    $scope.$location = $location;
    $scope.redirect = function (url, refresh) {
        if (refresh || $scope.$$phase) {
            $window.location.href = url;
        } else {
            $location.path(url);
            $scope.$apply();
        }
    };
    $scope.saveNew = function(){
        $.ajax({
            type: "POST",
            url: './data/save.php',
            data: { 'new': $scope.new },
            dataType: 'json',
            cache: false,
            success: function (response) {
              $scope.getFILE();
              $('#newdm').modal('hide');
            }
          });
    };
});