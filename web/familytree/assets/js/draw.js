export class Drawer {
    constructor(tree, rootNodes, canvas) {
        this.tree = tree;
        this.canvas = canvas;
        this.rootNodes = rootNodes;
        this.rowHeight = 100;
        this.boxHeight = 130;
        this.boxWidth = 80;
        this.boxPadding = 10;
        this.boxMargin = 10;
        this.boxWithPadding = this.boxWidth + this.boxPadding * 2 + this.boxMargin * 2;

        this.defaultMaleImage = 'assets/images/male.png';
        this.defaultFemaleImage = 'assets/images/female.png';
        this.calculateMaxBoxInRow();
    }

    refresh() {
        this.calculateMaxBoxInRow();
        this.draw();
    }

    setOnClick(editDetails, addMember, deleteMember) {
        this.editDetails = editDetails;
        this.addMember = addMember;
        this.deleteMember = deleteMember;
    }

    calculateMaxBoxInRow() {
        var index = 0;
        _.each(this.tree, (node, key) => {
            if (node.x > index) {
                index = node.x;
            }
        });
        this.widestRow = index;
    }

    draw() {
        this.rootNodes.forEach(index => {
            this.tree[index]['extraSpace'] = (this.boxWithPadding * this.widestRow) - this.boxWithPadding;
            if (this.tree[index].extraSpace < 0) {
                this.tree[index].extraSpace = this.tree[index].extraSpace * -1;
            }
            this.drawNode(index, 0, 0);
        });
        this.rootNodes.forEach(index => {
            this.drawB(index);
            this.tree[index].children.forEach(childIndex => {
                this.tree[childIndex].spouse.forEach(spouseIndex => {
                    this.drawB(spouseIndex);
                });
            });
        });
    }

    drawNode(index, spouseSpace, spaceRelativeToParent) {
        let node = this.tree[index];
        if (this.tree[index].xcord) {
            return;
        }

        if (spaceRelativeToParent > 0) {
            spaceRelativeToParent = spaceRelativeToParent - this.boxWithPadding / 2;
        }

        let canvasX = this.canvas.getBoundingClientRect().left;
        let canvasY = this.canvas.getBoundingClientRect().top;

        let xcord = this.boxWithPadding * node.x + spaceRelativeToParent + canvasX + spouseSpace;
        let ycord = this.boxWithPadding * node.y + this.rowHeight * node.y + canvasY;
        if (node.extraSpace) {
            xcord = xcord + node.extraSpace;
        }
        this.tree[index]['xcord'] = xcord;
        this.tree[index]['ycord'] = ycord;

        this.tree[index].spouse.forEach(spouseIndex => {
            this.drawNode(spouseIndex, spaceRelativeToParent, 0);
        });

        this.tree[index].children.forEach(childIndex => {
            this.drawNode(childIndex, 0, xcord);
        });

        //  this.drawBox(xcord, ycord, node,index);
    }

    drawB(index) {
        let node = this.tree[index];
        if (document.getElementById('family-node-' + index) != null) {
            return;
        }
        this.tree[index].children.forEach(childIndex => {
            this.drawB(childIndex);
        });
        this.drawBox(this.tree[index]['xcord'], this.tree[index]['ycord'], node, index);

    }

    drawBox(xcord, ycord, node, index) {

        let divNode = document.createElement('a');
        divNode.id = 'family-node-' + index;
        divNode.className = 'family-node bg-light '+ (node.gender=='f'?"node-f":"node-m")+' text-black';
        divNode.innerHTML = '<div class="name">' + node.name + '</div>';
        
        divNode.innerHTML = divNode.innerHTML + '<div class="info">' +
            '<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">' +
            '<button type="button" class="btn btn-light" onclick="$.addfm(\''+index+'\');"><i class="fa fa-plus"></i></button>' +
            '<button type="button" class="btn btn-light" onclick="$.showfm(\''+index+'\');"><i class="fa fa-edit"></i></button>' +
            '<button type="button" class="btn btn-light" onclick="$.rmfm(\''+index+'\');"><i class="fa fa-times"></i></button>' +
            ' </div></div>';

        if (!node.image && node.gender == 'f') {
            divNode.innerHTML = '<img class="img rounded-circle" src="' + this.defaultFemaleImage + '"/>' + divNode.innerHTML;
        }
        if (!node.image && node.gender == 'm') {
            divNode.innerHTML = '<img class="img rounded-circle" src="' + this.defaultMaleImage + '"/>' + divNode.innerHTML;
        }
        if(node.image){
            divNode.innerHTML = '<img class="img rounded-circle" src="./assets/images/' + node.image + '"/>' + divNode.innerHTML;
        }
        divNode.style.left = xcord;
        divNode.style.top = ycord;
        divNode.style.width = this.boxWidth;
        divNode.style.padding = this.boxPadding;
        divNode.style.marggin = this.boxMargin;
        this.canvas.append(divNode);

        this.drawRelation(xcord, ycord, node, index);

    }

    drawRelation(xcord, ycord, node, index) {

        node.parent.forEach(parentIndex => {
            let parent = this.tree[parentIndex];
            var div = document.createElement('div');
            div.className = 'family-tree-row';
            let width = parent.xcord - xcord;
            div.style.top = ycord - 15;

            if (width >= 0) {
                div.className += ' family-tree-row-left';
                div.style.width = width;
                div.style.left = xcord + this.boxWithPadding / 2;
            } else {
                div.className += ' family-tree-row-right';
                div.style.width = width * (-1);
                div.style.left = parent.xcord + this.boxWithPadding / 2;
            }
            this.canvas.append(div);

            if (document.querySelector('#family-parent-connect-' + parentIndex) != null) {
                return;
            }

            let divConnect = document.createElement('div');
            divConnect.className = 'family-tree-col';
            divConnect.id = 'family-parent-connect-' + parentIndex;
            let height = ycord - (parent.ycord + this.boxHeight + 15);
            divConnect.style.height = height;
            divConnect.style.left = parent.xcord + this.boxWithPadding / 2;
            divConnect.style.top = parent.ycord + this.boxHeight + 1;
            this.canvas.append(divConnect);

        });

        node.spouse.forEach(spouseIndex => {
            let spouse = this.tree[spouseIndex];

            if (!spouse.xcord) {
                return;
            }
            if (document.querySelector('#family-tree-row-' + spouseIndex) != null) {
                return;
            }
            if (document.querySelector('#family-tree-row-' + index) != null) {
                return;
            }
            var div = document.createElement('div');
            div.id = 'family-tree-row-' + spouseIndex;
            div.className = 'family-tree-row family-tree-plain-row';
            div.style.top = ycord + this.boxHeight / 2;
            if (spouse.xcord > node.xcord) {
                div.style.left = node.xcord + this.boxWidth + 21;
                div.style.width = spouse.xcord - this.boxWidth - node.xcord - 20;
            } else {
                div.style.left = spouse.xcord + this.boxWidth + 21;
                div.style.width = node.xcord - this.boxWidth - spouse.xcord - 20;
            }

            this.canvas.append(div);
        });
    }

    saveData(thisuuid,thistype){
        _.each(this.tree, (value, key) => { 
            delete value["x"]; 
            delete value["y"]; 
            delete value["previousSiblingChildCount"]; 
        }); 
        if(document.getElementById('tufile').value){
            var selectedFile = document.getElementById('tufile').value; 
            var imageData = $('.image-editor').cropit('export', { type: 'image/jpeg', quality: 1,  originalSize: false});
            var thistree = new Array;
            thistree=this.tree;
                $.ajax({
                    type: 'POST',
                    url: './assets/php/save_family.php?type=image',
                    data: {"uuid":thisuuid,"type":thistype,"filename":selectedFile.name,"file":imageData}
                }).done(function() {
                    $.ajax({
                        type: 'POST',
                        url: './assets/php/save_family.php',
                        data: {"uuid":thisuuid,"type":thistype,"tree":JSON.stringify(thistree)}
                    }).done(function() {
                        $('#newmember').modal('hide');
                        $("#reply").show().fadeOut(10000);
                        $.growl.notice({ title: "Data saved!", message: "Please wait..."  });
                    });
                });
        } else {
            $.ajax({
                type: 'POST',
                url: './assets/php/save_family.php',
                data: {"uuid":thisuuid,"type":thistype,"tree":JSON.stringify(this.tree)}
            }).done(function() {
                $('#newmember').modal('hide');
                $("#reply").show().fadeOut(10000);
                $.growl.notice({ title: "Data saved!", message: "Please wait..." });
            });
        }
     
    }

}