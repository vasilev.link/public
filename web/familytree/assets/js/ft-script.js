function getFile(thisdiv){ $("#"+thisdiv).click(); };
function sub(obj,thisdiv){  var file = obj.value;   var fileName = file.split("\\");  $("#"+thisdiv).html(fileName[fileName.length-1]);};
$(document).ready(function() {
  if($('.image-editor')[0]) {
    $('.image-editor').cropit({exportZoom: 1.5, allowDragNDrop: false});
    $('.prevbutton').click(function() {
      var imageData = $('.image-editor').cropit('export');
      window.open(imageData);
      return false;
    });
  }

});
