import { Drawer } from './draw.js';

export class FamilyTree {
    constructor(tree) {
        this.tree = tree;
        this.rootNodes = [];
        this.genRootNodes();
        this.genTree();
    }

    genRootNodes() {
        _.each(this.tree, (value, key) => {
            //  if (value.parent.length == 0) {
            if (!this.SpouseParent(value)) {
                this.rootNodes.push(key);
            }
            //  }
        }
        );
    }

    SpouseParent(element) {
        if (element.spouse.length == 0) {
            return false;
        }
        for (var i = 0; i < element.spouse.length; i++) {
            var spouseIndex = element.spouse[i];
            if (this.tree[spouseIndex].parent.length > 0) {
                return true;
            }
        }

        return false;
    }

    genTree() {
        this.rootNodes.forEach((elementIndex, index) => {
            this.genNode(elementIndex, 0 + index, 0);
        });
    }

    genNode(index, x, y) {
        if (this.tree[index].hasOwnProperty('x')) {
            return;
        }
        this.tree[index]['x'] = x;
        this.tree[index]['y'] = y;

        if (this.tree[index].spouse.length > 0) {
            this.tree[index].spouse.forEach((spouseIndex, index) => {
                this.genNode(spouseIndex, x + index + 1, y);
            });
        }


        if (this.tree[index].children.length > 0) {
            var previousChildSpouseCount = 0;
            var previousChildChildCount = 0;
            var parentSiblingChildCount = this.getPreviousSiblingChildCount(index);

            this.tree[index].children.forEach((childTreeIndex, index) => {
                var childX = index + previousChildSpouseCount + parentSiblingChildCount;
                this.tree[childTreeIndex]['previousSiblingChildCount'] = previousChildChildCount;
                this.genNode(childTreeIndex, childX, y + 1);
                previousChildSpouseCount = this.tree[childTreeIndex].spouse.length;
                previousChildChildCount = this.getChildCount(childTreeIndex);
            });
        }
    }

    getPreviousSiblingChildCount(index) {
        if (this.tree[index].previousSiblingChildCount) {
            return this.tree[index].previousSiblingChildCount;
        }

        if (this.tree[index].spouse.length > 0) {
            for (var i = 0; i < this.tree[index].spouse.length; i++) {
                var spouseIndex = this.tree[index].spouse[i];
                if (this.tree[spouseIndex].previousSiblingChildCount) {
                    return this.tree[spouseIndex].previousSiblingChildCount;
                }
            }
        }
        return 0;
    }

    getChildCount(index) {
        if (this.tree[index].children.length > 0) {
            return this.tree[index].children.length
        }
        if (this.tree[index].spouse.length > 0) {
            var total = 0;
            this.tree[index].spouse.forEach(spouseIndex => {
                total = total + this.tree[spouseIndex].children.length;
            });
            return total;
        }
        return 0;
    }

    render(canvas) {
        let $drawer = new Drawer(this.tree, this.rootNodes, canvas);
        $drawer.draw();
    }

    saveData(canvas, thisuuid = null, thistype = null) {
        let $drawer = new Drawer(this.tree, this.rootNodes, canvas);
        $drawer.saveData(thisuuid, thistype);
    }
}