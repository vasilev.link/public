import {FamilyTree} from './family-tree.js';
function removearr(array, element) {
    const index = array.indexOf(element);
    if (index !== -1) {
      array.splice(index, 1);
    }
}
function diff_years(dt2, dt1) 
 {
  var diff =(dt2.getTime() - dt1.getTime()) / 1000;
   diff /= (60 * 60 * 24);
  return Math.abs(Math.round(diff/365.25));
 }
$(document).ready(function () {
  $( ".datepicker" ).datepicker({ dateFormat: 'yy/mm/dd' });
   $.ajax({
    dataType: 'json',
    success: function (data) {
       localStorage.setItem('dataft', JSON.stringify(data));
       var drawer = new FamilyTree(data);
       drawer.render(document.getElementById('ftdiv'));
    },
    error: function () {
      console.log("error getting the data");
    },
    url: './assets/php/save_family.php'
  });
});
$.addfm = function(thisid){
  document.getElementById('ft-previd').value=thisid;
  document.getElementById('ft-name').value="";
  document.getElementById('ft-born').value="";
  document.getElementById('ft-died').value="";
  document.getElementById('ft-gender').value="m";
  document.getElementById('ft-relation').value="parent";
  document.getElementById('ft-picture').value="";
  $("#saveft").show(); $("#savefm").hide();
  $(".relations").show();
  $("#membercontent").html("");
  $('#newmember').modal('show');
};
document.getElementById("saveft").onclick = function() {
  var dataftold = JSON.parse(localStorage.getItem('dataft'));
  let uuid = Math.random().toString(36).substring(2, 15); 
  var dataftnew={};
  dataftnew[uuid]={};
  dataftnew[uuid]['children']= new Array;
  dataftnew[uuid]['name']=document.getElementById('ft-name').value;
  dataftnew[uuid]['born']=document.getElementById('ft-born').value;
  dataftnew[uuid]['gender']=document.getElementById('ft-gender').value;
  dataftnew[uuid]['died']=document.getElementById('ft-died').value;
  dataftnew[uuid]['location']=document.getElementById('ft-location').value;
  dataftnew[uuid]['spouse']=  new Array;
  dataftnew[uuid]['parent']=  new Array;
  if(document.getElementById('ft-relation').value=="child" && document.getElementById('ft-previd').value!==null){
    dataftnew[uuid]['parent'].push(document.getElementById('ft-previd').value);
    dataftold[document.getElementById('ft-previd').value]['children'].push(uuid);
  }
  if(document.getElementById('ft-relation').value=="spouse"){
    dataftold[document.getElementById('ft-previd').value]['spouse'].push(uuid);
    dataftnew[uuid]['spouse'].push(document.getElementById('ft-previd').value);
  }
  if(document.getElementById('ft-relation').value=="parent"){
    dataftold[document.getElementById('ft-previd').value]['parent'].push(uuid);
    dataftnew[uuid]['children'].push(document.getElementById('ft-previd').value);
  } 
  if (document.getElementById('tufile').value) {
    dataftnew[uuid]['image'] =  uuid + ".jpg";
  } else {
    dataftnew[uuid]['image'] = null;
  }
  if(document.getElementById('ft-relation').value=="parent"){
    var dataft =Object.assign(dataftnew,dataftold);
  } else {
    var dataft =Object.assign(dataftold,dataftnew);
  } 
    var drawer = new FamilyTree(dataft);
    drawer.saveData("",uuid,"new");
    //drawer.render(document.getElementById('ftdiv'));
    setTimeout(function(){   window.location.reload(1); }, 2000);
};
document.getElementById("savefm").onclick = function() {
  var dataft = JSON.parse(localStorage.getItem('dataft'));
  var uuid=document.getElementById("ft-thisid").value;
  dataft[uuid]['name']=document.getElementById('ft-name').value;
  dataft[uuid]['born']=document.getElementById('ft-born').value;
  dataft[uuid]['died']=document.getElementById('ft-died').value;
  dataft[uuid]['gender']=document.getElementById('ft-gender').value;
  dataft[uuid]['location']=document.getElementById('ft-location').value;
  if (document.getElementById('tufile').value) {
    dataft[uuid]['image'] =  uuid + ".jpg";
  } 
  if (document.getElementById('ft-imgurl').value){
    dataft[uuid]['image'] =  document.getElementById('ft-imgurl').value;
  } else {
    dataft[uuid]['image'] = null;
  }
  var drawer = new FamilyTree(dataft);
  drawer.saveData("",uuid,"update");
  $('#newmember').modal('hide');
  setTimeout(function(){   window.location.reload(1); }, 2000);
};
$.rmfm = function(thisid){ 
  var dataft = JSON.parse(localStorage.getItem('dataft'));
  if(!_.isEmpty(dataft[thisid].children))
  {
    _.each(dataft[thisid].children, (value,key)=>{
      delete dataft[value]; 
    }); 
  }
  delete dataft[thisid]; 
  _.each(dataft, (value, key) => { 
     if(value.spouse){
      removearr(value.spouse,thisid);    
     }
     if(value.children!=""){ 
      removearr(value.children,thisid);  
     }
     if(value.parent!=""){ 
      removearr(value.parent,thisid);            
     }
  });
  var drawer = new FamilyTree(dataft);
  drawer.saveData("",thisid,"delete");
  setTimeout(function(){   window.location.reload(1); }, 2000);
}
$.showfm = function(thisid){
  var dataft = JSON.parse(localStorage.getItem('dataft'));
  $("#saveft").hide(); $("#savefm").show();
  $(".relations").hide();
  let divmember = document.getElementById('membercontent');
  let image = "male.png"; 
  if(dataft[thisid]['gender'] == 'f'){ image = "female.png"; }
  if(dataft[thisid]['image'] !== null){ image = dataft[thisid]['image']; }
  var age = 0;
  if(dataft[thisid]['born']){
    var dt1 = new Date(dataft[thisid]['born']);
    if(dataft[thisid]['died']){
      var dt2 = new Date(dataft[thisid]['died']);
    } else {
      var dt2 = new Date(); 
    }
    age = diff_years(dt1, dt2);
  }
  document.getElementById("ft-name").value = dataft[thisid]['name']; 
  document.getElementById("ft-born").value = (dataft[thisid]['born']!==null?dataft[thisid]['born']:"");
  document.getElementById("ft-died").value = (dataft[thisid]['died']!==null?dataft[thisid]['died']:"");
  document.getElementById("ft-thisid").value = thisid; 

  divmember.innerHTML = '<input type="hidden" id="ft-imgurl" value="'+(dataft[thisid]['image'] !== null?dataft[thisid]['image']:"")+'">'+' <img src="./assets/images/'+image+'" class="img img-fluid rounded-circle">'+
    '<br>Age:'+age+'<div style="width:100%;height:10px;border-bottom:2px solid #ddd;"></div><br>';
    
  
    if(dataft[thisid]['location']!==null){
      document.getElementById("ft-location").value = dataft[thisid]['location']; 
    }
    document.getElementById("ft-gender").value = dataft[thisid]['gender']; 
    $( ".datepicker" ).datepicker({ dateFormat: 'yy/mm/dd' });
    $('#newmember').modal('show');

}
