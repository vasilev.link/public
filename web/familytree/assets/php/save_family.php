<?php
if ($_GET["type"] == "image") {
    if ($_POST["type"] == "new" || $_POST["type"] == "update") {
        list($type, $data) = explode(';', $_POST['file']);
        list(, $data) = explode(',', $data);
        $file_data = base64_decode($data);
        $finfo = finfo_open();
        $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);
        if ($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg') {
            $file_type = 'jpg';
        } else if ($file_mime_type == 'image/png') {
            $file_type = 'png';
        } else if ($file_mime_type == 'image/gif') {
            $file_type = 'gif';
        } else {
            $file_type = 'other';
        }
        if (in_array($file_type, ['jpg', 'png', 'gif'])) {
            file_put_contents("../images/".htmlspecialchars($_POST["uuid"]).".".$file_type, $file_data);
        } else {
            echo 'Error : Only JPEG, PNG & GIF allowed';
        }
    }
} else {
    if (isset($_POST['tree']) and !empty($_POST['tree'])) {
        if ($_POST["type"] == "delete" && !empty($_POST["uuid"])) {
            $files = glob('../images/' . htmlspecialchars($_POST["uuid"]) . '*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                    echo $file . " deleted!";
                }
            }
        }
        file_put_contents("../data/family_tree.json", $_POST['tree']);
        echo $_POST['tree'];
    } else {
        header('Content-Type: application/json');
        if(file_exists("../data/family_tree.json")){
            $file = file_get_contents("../data/family_tree.json", true);
            echo $file;
        } else {
            $uuid=substr(str_shuffle(MD5(microtime())), 0, 12);
            $ftarr=Array(
                $uuid=>Array(
                    'name'=>'Change me',
                    'children'=>Array(),
                    'parent'=>Array(),
                    'spouse'=>Array(),
                    'image'=>null,
                    'born'=>'',
                    'died'=>'',
                    'gender'=>'m',
                    'location'=>''
                )
            );
            echo json_encode($ftarr,true);
            file_put_contents("../data/family_tree.json", json_encode($ftarr,true));
        }
    }
}
