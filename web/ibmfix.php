<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
    header('Content-Type: text/json; charset=utf-8');
    $dom = new DOMDocument();
    $data=array();
   $i=0;
    libxml_use_internal_errors(true);
   $html = $dom->loadHTMLFile("https://www.ibm.com/support/pages/recommended-fixes-ibm-mq");
   $dom->preserveWhiteSpace = false; 
   $tables = $dom->getElementsByTagName('table'); 
   $rows = $tables->item(0)->getElementsByTagName('tr'); 
  foreach ($rows as $row) 
  {  
      $cols = $row->getElementsByTagName('td'); 
      $data["version"]=str_replace(array("IBM MQ Continuous Delivery (CD) release","Product & Version","Latest Release"),"",$cols->item(0)->nodeValue); 
      $data["fixpack"]=str_replace(array("Fix Pack","Latest Release","\n "," "),"",$cols->item(1)->nodeValue); 
     if(!empty($data["version"]) && !empty($data["fixpack"])){ $newdata[]=$data; }
    $i++;
    }

echo json_encode($newdata, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
/*
foreach($newdata as $key=>$val){
 echo $val["fixpack"]; 
}
*/