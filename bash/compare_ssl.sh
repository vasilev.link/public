#!/bin/bash

DOMAIN=$1
CER_FILE=$2
PROXY=$3

if [ -z "$CER_FILE" ]
then
    echo "Usage:" >&2
    echo "  $(basename "$0") DOMAIN CERT_FILE" >&2
    exit 1
fi
if [ ! -f "$CER_FILE" ]
then
    echo "Usage:" >&2
    echo "  $(basename "$0") DOMAIN CERT_FILE" >&2
    exit 1
fi
if [[ ! -z "$PROXY" ]]
then
    PROXY="-proxy ${PROXY}"
fi

DOMAIN_REG=`echo "Q" | openssl s_client ${PROXY} -connect ${DOMAIN}:443 -servername ${DOMAIN} 2> /dev/null | openssl x509 -noout -dates | grep notBefore`
DOMAIN_REG=${DOMAIN_REG/notBefore=/}
DOMAIN_REG=`date --date="${DOMAIN_REG}" --utc +"%Y%m%d.%H%M"`
echo "Domain registration date: ${DOMAIN_REG}"

CERT_REG=`cat ${CER_FILE} | openssl x509 -noout -dates | grep notBefore`
CERT_REG=${CERT_REG/notBefore=/}
CERT_REG=`date --date="${CERT_REG}" --utc +"%Y%m%d.%H%M"`
echo "Cert   registration date: ${CERT_REG}"

if [ "$DOMAIN_REG" = "$CERT_REG" ]; then
    echo "Certificates are the same. No need to download again."
else
    echo -n | openssl s_client ${PROXY} -connect ${DOMAIN}:443 | \
      sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > $CER_FILE
fi