#script for manipulating big load of messages in one or many Qmanagers
#created by V.Vasilev
#https://vasilev.link

QM=`dspmq | sed -n "s/.*QMNAME(\([^ ]*\)).*(Running)$/\1/p"`
if [ -z "$QM" ]; then
  echo "QMGR(s) not available"; exit 1
fi
LISTQUEUES=$2
THISQMANAGER=$3
MQBACKUPDIR=/data/mq/QMbackup
case "$1" in
	help )
	     echo ""
	     echo "Queue Backup/Restore Script"
	     echo ""
	     echo "usage:"
	     echo "   -  $0 changeq list get/put enabled/disabled QMANAGER_NAME filewithqueues - set get/put enabled/inhibited for list of queues"
	     echo "   -  $0 changeq app get/put enabled/disabled QMANAGER_NAME APP_CODE - set get/put enabled/inhibited for list of queues"
	     echo "   -  $0 load QManager Queue Folder_with_files Extension_for_files - load messages from files"
	     echo "   -  $0 backup - backup all non-system queues but not deleting the messages"
	     echo "   -  $0 backup queue1,queue2,queue3 qmanager - backup queues with separator ',' on given Qmanager but not deleting the messages"
	     echo "   -  $0 backup all qmanager - backup all all non-system queues on given Qmanager but not deleting the messages"
	     echo "   -  $0 backupall - backup all queues but not deleting the messages"
	     echo "   -  $0 empty - backup all non-system queues and deleting the messages"
	     echo "   -  $0 empty queue1,queue2,queue3 qmanager - backup queues with separator ',' on given Qmanager and deleting the messages"
	     echo "   -  $0 empty all qmanager - backup all queues on given Qmanager and deleting the messages"
	     echo "   -  $0 emptyall - backup all queues and deleting the messages"
	     echo "   -  $0 delete - deleting the messages from all non-system queues without backup"
	     echo "   -  $0 delete queue1,queue2,queue3 qmanager - deleting the messages from queues with separator ',' on given Qmanager without backup"
	     echo "   -  $0 delete all qmanager - deleting the messages from all queues on given Qmanager without backup"
	     echo "   -  $0 deleteall - deleting the messages from all queues without backup"
	     echo "   -  $0 restore - check what is in folder "$MQBACKUPDIR" and restore all messages on all Qmanagers".
	     echo "   -  $0 restore queue1,queueu2,queue3 qmanager - check what is in folder "$MQBACKUPDIR"/qmanager and restore messages on given queues with separator','".
	     echo "   -  $0 restore all qmanager - check what is in folder "$MQBACKUPDIR"/qmanager and restore messages on all backuped queues".
	     echo "   -  $0 requeue - take the messages from Backout queues and send them in not Backout ones".
	     echo "   -  $0 requeue queue1,queue2,queue3 qmanager - take the messages from given Backout queues and send them in not Backout ones".
	     echo "   -  $0 requeue all qmanager - take the messages from all Backout queues on given Qmanager and send them in not Backout ones".
	     echo "   -  $0 QMGR(optional) APP_CODE(optional) - list queues"
	     echo ""
	     echo ""
	    ;;
	load )
		QMGR=$1
		QUEUE=$2
	    DIR=$3
		EXT=$4
		if [ -z "${QMGR}" ]; then
         echo "Usage: $0 QManager Queue Folder_with_files Extension_for_files"
         exit 1
        fi
		COUNT=0
        mkdir -p $DIR/processed
        cd $DIR
        echo *.$EXT | xargs ls | while read FILE; do
          (( COUNT++ ))
          q -m $QMGR -o $QUEUE -F $DIR/${FILE##*/} > /dev/null
          mv $DIR/${FILE##*/} $DIR/processed
          echo "processing file:" ${FILE##*/}
        done
        echo "total number of messages processed:" $COUNT	
	   ;;
	changeq )
	    CHANGETYPE=$2
	    THECASE=$3
		ACTION=$(echo $4 | tr '[:lower:]' '[:upper:]')
        QMGR=$5
		if [ "${CHANGETYPE}" == "app" ]; then
		APPNAME=$6
		echo "Collecting Queues to disable from QueueManager $QM"
	    queue_list="$(echo "dis ql($APPNAME*)" | runmqsc $QMGR | sed -n '/QUEUE(/{s/^.*QUEUE(\([^   ]*\)).*/\1/;p;}' )"
		echo $queue_list
		for i in $queue_list ; do
        if [ -n "$i" ]
          then
            echo "#Processing queue : $i  Action $THECASE $ACTION ";
            echo "alter ql($i) $THECASE($ACTION)" | runmqsc $QMGR
		  fi
    	done
		elif [ "${CHANGETYPE}" == "list" ]; then
        LISTQUEUES=$6
		if [ -z "$LISTQUEUES" ]
		then
          echo "usage: $0 get/put enabled/disabled QMANAGER_NAME filewithqueues.txt"
          exit 1
		fi
		while read THISQUEUE; do 
          echo "alter qlocal("${THISQUEUE##*/}") $THECASE($ACTION)" | runmqsc $QMGR
          sleep 1
		done < $LISTQUEUES
		fi
	    ;;
    backup )
		if [ "${LISTQUEUES}" == "" ]; then
        for J in ${QM}
          do
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | grep -v SYSTEM | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}'`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$J" ]; then
              mkdir -p $MQBACKUPDIR/$J
            fi
            rm -Rf $MQBACKUPDIR/$J/$Q.msg
            qload -m $J -i $Q -f $MQBACKUPDIR/$J/$Q.msg
            echo "creating file:" $MQBACKUPDIR/$J/$Q.msg "...done"
          done
        done
		elif [ "${LISTQUEUES}" == "all" ]; then
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $THISQMANAGER | grep -v SYSTEM | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}'`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$THISQMANAGER" ]; then
              mkdir -p $MQBACKUPDIR/$THISQMANAGER
            fi
            rm -Rf $MQBACKUPDIR/$THISQMANAGER/$Q.msg
            qload -m $THISQMANAGER -i $Q -f $MQBACKUPDIR/$THISQMANAGER/$Q.msg
            echo "creating file:" $MQBACKUPDIR/$THISQMANAGER/$Q.msg "...done"
          done
		else
          arrq=$(echo $LISTQUEUES | tr "," "\n")
          for xq in $arrq
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$THISQMANAGER" ]; then
              mkdir -p $MQBACKUPDIR/$THISQMANAGER
            fi
            rm -Rf $MQBACKUPDIR/$THISQMANAGER/$xq.msg
            qload -m $THISQMANAGER -i $xq -f $MQBACKUPDIR/$THISQMANAGER/$xq.msg
            echo "creating file:" $MQBACKUPDIR/$THISQMANAGER/$xq.msg "...done"
         done
       fi
       ;;
    backupall )
        for J in ${QM}
          do
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}' | sed "s/[ ]*$//"`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$J" ]; then
              mkdir -p $MQBACKUPDIR/$J
            fi
            rm -Rf $MQBACKUPDIR/$J/$Q.msg
            qload -m $J -i $Q -f $MQBACKUPDIR/$J/$Q.msg
            echo "creating file:" $MQBACKUPDIR/$J/$Q.msg "...done"
          done
        done
        ;;
    empty )
		if [ "${LISTQUEUES}" == "" ]; then
        for J in ${QM}
          do
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | grep -v SYSTEM | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}'`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$J" ]; then
              mkdir -p $MQBACKUPDIR/$J
            fi
			rm -Rf $MQBACKUPDIR/$J/$Q.msg
            qload -m $J -I $Q -f $MQBACKUPDIR/$J/$Q.msg
            echo "creating file:" $MQBACKUPDIR/$J/$Q.msg "...done"
          done
        done
		elif [ "${LISTQUEUES}" == "all" ]; then
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $THISQMANAGER | grep -v SYSTEM | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}'`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$THISQMANAGER" ]; then
              mkdir -p $MQBACKUPDIR/$THISQMANAGER
            fi
            rm -Rf $MQBACKUPDIR/$THISQMANAGER/$Q.msg
            qload -m $THISQMANAGER -I $Q -f $MQBACKUPDIR/$THISQMANAGER/$Q.msg
            echo "creating file:" $MQBACKUPDIR/$THISQMANAGER/$Q.msg "...done"
          done
		else
          arrq=$(echo $LISTQUEUES | tr "," "\n")
          for xq in $arrq
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$THISQMANAGER" ]; then
              mkdir -p $MQBACKUPDIR/$THISQMANAGER
            fi
            rm -Rf $MQBACKUPDIR/$THISQMANAGER/$xq.msg
            qload -m $THISQMANAGER -I $xq -f $MQBACKUPDIR/$THISQMANAGER/$xq.msg
            echo "creating file:" $MQBACKUPDIR/$THISQMANAGER/$xq.msg "...done"
          done
        fi
        ;;
    emptyall )
        for J in ${QM}
          do
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}' | sed "s/[ ]*$//"`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$J" ]; then
              mkdir -p $MQBACKUPDIR/$J
            fi
            rm -Rf $MQBACKUPDIR/$J/$Q.msg
            qload -m $J -I $Q -f $MQBACKUPDIR/$J/$Q.msg
            echo "creating file:" $MQBACKUPDIR/$J/$Q.msg "...done"
          done
        done
        ;;
	delete )
		if [ "${LISTQUEUES}" == "" ]; then
		read -r -p "Are you sure you want to delete all messages from all non-system queues without backup them? [y/N] " response
		if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
		then
        for J in ${QM}
          do
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | grep -v SYSTEM | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}'`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$J" ]; then
              mkdir -p $MQBACKUPDIR/$J
            fi
            qload -m $J -I $Q
            echo "deleting messages from queue:" $Q "on Qmanager:" $J "...done"
          done
          echo ""
          echo "Are you crazy man? You deleted all messages.. no way back."
        done
		else
		echo "OK,nothing happened. Next time think first :)"
		fi
		elif [ "${LISTQUEUES}" == "all" ]; then
		read -r -p "Are you sure you want to delete all messages from all non-system queues on Qmanager:"$THISQMANAGER" without backup them? [y/N] " response
		if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
		then
          THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $THISQMANAGER | grep -v SYSTEM | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}'`;
          for Q in ${THISQUEUE}
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$THISQMANAGER" ]; then
              mkdir -p $MQBACKUPDIR/$THISQMANAGER
            fi
            qload -m $THISQMANAGER -I $Q
            echo "deleting messages from queue:" $Q "on Qmanager:" $THISQMANAGER "...done"
          done
          echo ""
          echo "Are you crazy man? You deleted all messages.. no way back."
		else
		echo "OK,nothing happened. Next time think first :)"
		fi
		else
		read -r -p "Are you sure you want to delete all messages from "$LISTQUEUES" on Qmanager: "$THISQMANAGER" without backup them? [y/N] " response
		if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
		then
          arrq=$(echo $LISTQUEUES | tr "," "\n")
          for xq in $arrq
            do
            if [ ! -d "$MQBACKUPDIR" ]; then
              mkdir -p $MQBACKUPDIR
            fi
            if [ ! -d "$MQBACKUPDIR/$THISQMANAGER" ]; then
              mkdir -p $MQBACKUPDIR/$THISQMANAGER
            fi
            qload -m $THISQMANAGER -I $xq
            echo "deleting messages from queue:" $xq "on Qmanager:" $THISQMANAGER "...done"
          done
          echo ""
          echo "Are you crazy man? You deleted all messages.. no way back."
		else
		echo "OK,nothing happened. Next time think first :)"
		fi
        fi
        ;;
    deleteall )
	    read -r -p "Are you sure you want to delete all messages from all queues without backup them? [y/N] " response
	    if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
	     then
         for J in ${QM}
           do
           THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | sed -n '/QUEUE(/{N;s/\n/\ /;s/^.*QUEUE(\([^ ]*\)).*CURDEPTH(\(.*\))/\1/;p;}' | sed "s/[ ]*$//"`;
           for Q in ${THISQUEUE}
             do
             if [ ! -d "$MQBACKUPDIR" ]; then
               mkdir -p $MQBACKUPDIR
             fi
             if [ ! -d "$MQBACKUPDIR/$J" ]; then
               mkdir -p $MQBACKUPDIR/$J
             fi
             qload -m $J -I $Q
             echo "deleting messages from queue:" $Q "on Qmanager:" $J "...done"
           done
         done
		 echo ""
		 echo "Are you crazy man? You deleted all messages.. no way back."
	   else
         echo "OK,nothing happened. Next time think first :)"
	   fi
       ;;
	restore )
       if [ "${LISTQUEUES}" == "" ]; then
         for i in $(ls -d $MQBACKUPDIR/*)
         do
           for j in `ls -la $i/| grep .msg | awk '{print $9}'`
             do
             qload -f $i/$j -m ${i//$MQBACKUPDIR\/} -o ${j%.msg}
             mv $i/$j $i/${j/.msg/.restored}
             echo "restoring messages in Queue:" ${j%.msg} "on Qmanager:" ${i//$MQBACKUPDIR\/} "...done"
           done
         done
	   elif [ "${LISTQUEUES}" == "all" ]; then
         for j in `ls -la $MQBACKUPDIR/$THISQMANAGER/| grep .msg | awk '{print $9}'`
           do
           qload -f $MQBACKUPDIR/$THISQMANAGER/$j -m ${THISQMANAGER//$MQBACKUPDIR\/} -o ${j%.msg}
           mv $MQBACKUPDIR/$THISQMANAGER/$j $MQBACKUPDIR/$THISQMANAGER/${j/.msg/.restored}
           echo "restoring messages in Queue:" ${j%.msg} "on Qmanager:" ${THISQMANAGER//$MQBACKUPDIR\/} "...done"
         done
	   else
         arrq=$(echo $LISTQUEUES | tr "," "\n")
         for xq in $arrq
           do
           qload -f $MQBACKUPDIR/$THISQMANAGER/$xq.msg -m ${THISQMANAGER//$MQBACKUPDIR\/} -o ${xq%.msg}
           mv $MQBACKUPDIR/$THISQMANAGER/$xq.msg $MQBACKUPDIR/$THISQMANAGER/$xq.restored
           echo "restoring messages in Queue:" ${xq%.msg} "on Qmanager:" ${THISQMANAGER//$MQBACKUPDIR\/} "...done"
         done
	   fi
       ;;
	requeue )
	   if [ "${LISTQUEUES}" == "" ]; then
         for J in ${QM}
           do
           THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $J | grep "\.BO." | sed 's/.*QUEUE//' | tr -d '()' | awk '{ print $1 }'`;
           for Q in ${THISQUEUE}
             do
             qload -I $Q -m $J -o ${Q/.BO./.}
             echo "requeue messages from queue:" $Q "...done"
           done
         done
	   elif [ "${LISTQUEUES}" == "all" ]; then
	     THISQUEUE=`echo "dis ql(*) curdepth WHERE(CURDEPTH GT 0)"| runmqsc $THISQMANAGER | grep "\.BO." | sed 's/.*QUEUE//' | tr -d '()' | awk '{ print $1 }'`;
         for Q in ${THISQUEUE}
           do
           qload -I $Q -m $THISQMANAGER -o ${Q/.BO./.}
           echo "requeue messages from queue:" $Q "...done"
         done
	   else
         arrq=$(echo $LISTQUEUES | tr "," "\n")
         for xq in $arrq
           do
           qload -I $xq -m $THISQMANAGER -o ${xq/.BO./.}
           echo "requeue messages from queue:" $xq "...done"
         done
       fi
       ;;
    * )
       if [ -z "$1" ]; then
	     QM=`dspmq | sed -n 's/.*QMNAME(\([^)]*\)).*(Running)$/\1/p'`
	   else
	     QM="$1"
	   fi
	   if [ -z "$2" ]; then
	     QL=""
	   else
	     QL="$2"
	   fi
	   for J in $QM; do
         echo; echo "           $J"
         echo "DIS QL($QL*) CURDEPTH WHERE(CURDEPTH GT 0)"|/usr/bin/runmqsc $J |\
         sed -n '/QUEUE(/{N;s/^.*QUEUE(\([^)]*\)).*CURDEPTH(\([^)]*\)).*$/         \2  \1/;s/ *\(.\{10,\}\) /\1 /p;}' | sort -r
       done;
       ;;
    esac
