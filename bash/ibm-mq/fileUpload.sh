QMNAME=$1
QNAME=$2
FILES=$3
echo "###################"
echo "Running script using:"
id
echo "QManager=$QMNAME"
echo "QName=$QNAME"
echo "###################"
counter=0
for i in `ls $FILES`
do
    let counter=$counter+1
        echo q -m $QMNAME -o $QNAME -F $FILES/$i
        echo `q -m $QMNAME -o $QNAME -F $FILES/$i`
done
echo "####DONE####"
echo "processed $counter messages"
echo "####DONE####"
