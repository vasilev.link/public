QMGR=$1
PASSWORD=$2
LABEL=`echo "$1" | tr '[:upper:]' '[:lower:]'`
THRESHOLD_IN_DAYS="10"
KEYTOOL="runmqckm"
KEYSTORE="/var/mqm/qmgrs/$QMGR/ssl/key.kdb"
RET=0
ALIAS=ibmwebspheremq$LABEL

email_attachment() {
  boundary="_====_mqssl_====_$(date +%Y%m%d%H%M%S)_====_"
  mailfrom=""
  mailto=""
  {
  echo "From: $mailfrom"
  echo "To: $mailto"
  echo "Subject: [WARNING] certificate expiration"
  echo "Content-Type: multipart/mixed; boundary=\"$boundary\""
  echo "MIME-Version: 1.0"
  echo "--$boundary
Content-Type: text/html; charset=UTF8

Dear Team,<br>
The certificate $1 will expire on $2<br>
Please check:<br>
<br>
server: `hostname`<br>"
echo "--${boundary}--"
  }  | $MAILER $mailto
}

function check{
  CURRENT=`date +%s`
  THRESHOLD=$(($CURRENT + ($THRESHOLD_IN_DAYS*24*60*60)))
  if [ $THRESHOLD -le $CURRENT ]; then
   echo "[ERROR] Invalid date."
   exit 1
  fi
  $KEYTOOL -list -v -keystore "$KEYSTORE"  $PASSWORD 2>&1 > /dev/null
  if [ $? -gt 0 ]; then echo "Error opening the keystore."; exit 1; fi
  UNTIL=`$KEYTOOL -cert -details -label $ALIAS -db $KEYSTORE -type cms -pw $PASSWORD | grep Valid | sed -n -e 's/^.*To: //p'`
  UNTIL_SECONDS=`date -d "$UNTIL" +%s`
  REMAINING_DAYS=$(( ($UNTIL_SECONDS -  $(date +%s)) / 60 / 60 / 24 ))
  if [ $THRESHOLD -le $UNTIL_SECONDS ]; then
    echo "Certificate $ALIAS expires in '$UNTIL' ($REMAINING_DAYS day(s) remaining)."
  else
    email_attachment $ALIAS $UNTIL
    RET=1
  fi
}

if [ -z "$QMNAME" ]
then
    echo "usage: $0 QMANAGER_NAME KEYSTORE_PASSWORD"
    echo "script is checking the default keystore path for IBM MQ and default label - ibmwebspheremq<QMGR_NAME>"
    exit 1
fi
    
check
