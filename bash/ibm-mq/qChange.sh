#script for: setting get/put enabled/disabled for list of queues on given qmanager
#powered by Vasilev.link
thecase=$1
action=$2
qmgr=$3
listqueues=$4
if [ -z "$listqueues" ]
then
        echo "usage: $0 get/put enabled/disabled QMANAGER_NAME filewithqueues.txt"
        exit 1
fi
while read thisqueue; do
        echo "alter qlocal("${thisqueue##*/}") $thecase($action)" | runmqsc $qmgr
        sleep 1
done < $listqueues
