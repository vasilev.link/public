#! /bin/ksh
mailto=$4
mailcc=$5
QUEUE=$3
QMGR=$1
QMGRIP=$2
cd /monitoring_queues/
if [ ! -d "$QUEUE" ]; then
mkdir $QUEUE
fi
rm $QUEUE/log_err.txt
rm $QUEUE/log_tmp.txt
rm $QUEUE/alllog.txt
echo "" >> $QUEUE/alllog.txt
echo "" >> $QUEUE/log_err.txt
echo "There are some messages in $QUEUE . Please check them." >> $QUEUE/log_err.txt
echo "---------------------------------------------------" >> $QUEUE/log_err.txt
echo "" >> $QUEUE/log_err.txt
ExitCode=0
while [ $ExitCode = 0 ]
do
    /mqutl/latest/getq2fc $QUEUE/log_tmp.txt $QUEUE $QMGR ADMINMQ/TCP/$QMGRIP > $QUEUE/mqlog.tmp
    ExitCode=$?
    if [ $ExitCode = 0 ] 
    then
       echo `date` >> $QUEUE/log_err.txt
       cat $QUEUE/log_tmp.txt >> $QUEUE/log_err.txt
	echo "" >> $QUEUE/log_err.txt
	echo "---------------------------------------------------" >> $QUEUE/log_err.txt
	echo "" >> $QUEUE/log_err.txt
    else
	echo "no messages in $QUEUE at `date`" >  $QUEUE/log_tmp.txt
    fi
done
echo "" >> $QUEUE/log_err.txt
echo "---------------------------------------------------" >> $QUEUE/log_err.txt
echo "" >> $QUEUE/log_err.txt
if [ -s "$QUEUE/log_err.txt" -a $( wc -c < "$QUEUE/log_err.txt" ) -gt 340 ]
then
	echo "references for messages:" >> $QUEUE/alllog.txt
	echo "" >> $QUEUE/alllog.txt
       cat $QUEUE/log_err.txt | grep XREF >> $QUEUE/alllog.txt
	cat $QUEUE/log_err.txt | grep FCCREF >> $QUEUE/alllog.txt
	cat $QUEUE/log_err.txt | grep ACENTRYREF >> $QUEUE/alllog.txt
	echo "" >> $QUEUE/alllog.txt
	echo "messages:" >> $QUEUE/alllog.txt
	cat $QUEUE/log_err.txt >> $QUEUE/alllog.txt
	mail -s "ERROR MESSAGES on $QUEUE queue on `date`" -c "$mailcc" $mailto<$QUEUE/alllog.txt
fi
