#!/usr/bin/bash
#powered by Vasilev.link
#create file named channel name with all current connections
#parse runmqsc output to custom output - json, comma separated, whatever

QM=`dspmq | sed -n "s/.*QMNAME(\([^ ]*\)).*(Running)$/\1/p"`
WORKINGDIR=/var/tmp/checkconn

if [ -z "$QM" ]; then
  echo "QMGR(s) not available"; exit 1
fi
if [ ! -d "$WORKINGDIR" ]; then
  mkdir -p $WORKINGDIR
fi
for J in ${QM}
  do
    echo "dis chs(*) rappltag rversion where(chltype eq svrconn)" | runmqsc $J |  egrep "CHANNEL|CONNAME|RAPPLTAG|RVERSION" | awk '{line=line " " $0} NR%4==0{print substr(line,2); line=""}' | while read line; do
           line=${line//CURRENT/}
           line=${line//STATUS(RUNNING)/}
           line=${line//SUBSTATE(RECEIVE)/}
           line=${line//CHLTYPE(SVRCONN)/}
           channel=`echo $line|sed 's/.*CHANNEL//' | tr -d '()' | awk '{ print $1 }'`;
           conname=`echo $line|sed 's/.*CONNAME//' | tr -d '()' | awk '{ print $1 }'`;
           version=`echo $line|sed 's/.*RVERSION//' | tr -d '()' | awk '{ print $1 }'`;
           apptag=`echo $line|grep -oP '(?<=RAPPLTAG).*?(?=RVERSION)' | tr -d '()' `;
           echo "channel:$channel,conname:$conname,version:$version,apptag:$apptag" >> ${WORKINGDIR}/${channel}.log
    done
done
