rem script for unloading messages from queue to file on windows
rem powered by Vasilev.link and T.Yovkov
echo off
setlocal EnableDelayedExpansion
set MQCHL=%1%
set MQHOST=%2%
set MQPORT=%3%
set MQQMGR=%4%
set MQQUEUE=%5%
set BACKUPDIR=%6%
IF not exist %BACKUPDIR%\queue (mkdir %BACKUPDIR%\queue)
IF not exist %BACKUPDIR%\messages (mkdir %BACKUPDIR%\messages)
set MQSERVER=%MQCHL%/TCP/%MQHOST%(%MQPORT%)
set SAVESTAMP=%DATE:/=-%
echo display ql(%MQQUEUE%) curdepth | runmqsc %MQQMGR% | find "CURDEPTH" > %BACKUPDIR%\queue\curdepth.txt
q -c %MQQMGR% -i %MQQUEUE% -ddf3 | find "Message Id" > %BACKUPDIR%\queue\Ids.txt
for /f "delims=" %%a in (%BACKUPDIR%\queue\Ids.txt) do (
rem 	echo %%a
	set MID=%%a
	set MID=!MID:~14,48!
 	echo !MID!
	rem get message by message id
	q -c %MQQMGR% -i %MQQUEUE% -gxm!MID! -F %BACKUPDIR%\messages\message_%SAVESTAMP%_!MID!.txt
)
exit /b
