## Usage example
<br/><br/>
login to **ansible_server**<br/>
#with your user<br/>
cd<br/>
mkdir ansible<br/>
cd ansible<br/>
git clone https://link-to-repository.git<br/>
<br/>
### Install mqfte agent
<br/>
sudo su - **ansible_user**<br/>
cd /home/youruser/ansible/mft/<br/>
ansible-playbook install_mqfte.yml -i inventories/mqfte_servers.ini --limit servername --check<br/>
#in case no errors<br/>
ansible-playbook install_mqfte.yml -i inventories/mqfte_servers.ini --limit servername #or where you want to install it ..<br/>
<br/>
<br/>
### Upgrade mqfte agent
<br/>
#similar for mqfte upgrade
ansible-playbook upgrade_mqfte.yml -i inventories/mqfte_servers.ini --limit TEST/or server --check<br/>
