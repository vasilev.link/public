#script for: deleting monitors,deleting transfers, exporting monitors, importing monitors, renaming agent, renaming agent in files
#you should specify parameters $0 help for more information
#created by V.Vasilev
#https://vasilev.link
thecase=$1
archivedir="/backupdir" #folder with read/write permissions of mqm
AGENT_DIR="/agentdir" #folder where agent is installed
AGENT_CERTDIR="/agentcertdir" #folder where certificates for the agent are installed
case "$1" in
	help )
	echo "tool for managing FTE monitors and agents"
	echo "usage: $0 deletemonitors AGENT_NAME QMANAGER_NAME APPLICATION(optional)"
	echo "usage: $0 deletetransfers agent Qmanager transfer_uuid"
	echo "usage: $0 exportmonitors AGENT_NAME(optional)"
	echo "usage: $0 importmonitors MONITORS_DIR AGENT(to delete monitors - optional) QMGR(optional)"
	echo "usage: $0 renameagent Config_Qmanager Old_Agent_name New_Agent_name"
	echo "usage: $0 renagentinfiles OLD_AGT_NAME NEW_AGT_NAME directory_to_search"
	;;
    deletemonitors )
		mqtemp="/tmp"
		AGTNAME=$2
		QMNAME=$3
		APPLICATION=$
		if [ -z "$QMNAME" ]
		then
			echo "usage: $0 deletemonitors AGENT_NAME QMANAGER_NAME"
			exit 1
		fi
		if [ -z "$APPLICATION" ]
		then
		fteListMonitors -ma $AGTNAME > $mqtemp/list.txt
		else
		fteListMonitors -ma $AGTNAME | grep $APPLICATION > $mqtemp/list.txt
		fi
		while read line
		do
		AGENT=$(echo $line | cut -d" " -f1)
		MONIT=$(echo $line | cut -d" " -f2)
		echo "deleting monitor $MONIT from agent $AGENT"
		fteDeleteMonitor -ma $AGENT -mm $QMNAME -mn $MONIT
		done < $mqtemp/list.txt
		rm $mqtemp/list.txt
	;;
	deletetransfers )
		AGENT=$2
		QMGR=$3
		UUID=$4
		trtype=$5
		if [ -z "$UUID" ]
		then
			echo "usage: $0 deletetransfers agent Qmanager transfer_uuid TYPE(optional-processing,in queue,started...)"
			exit 1
		fi| 
		if [ -z "$trtype" ]
		then
			fteShowAgentDetails -v $AGENT | grep $UUID | while read uuid rest
			do 
				fteCancelTransfer -a $AGENT -m $QMGR $uuid
				echo "$uuid canceled."
			done
		else
			fteShowAgentDetails -v $AGENT | grep $UUID | grep $trtype | while read uuid rest
			do 
				fteCancelTransfer -a $AGENT -m $QMGR $uuid
				echo "$uuid -> $trtype canceled."
			done
		fi
	;;
	exportmonitors )
		THISAGENT=$2
		mqbackupdir=$archivedir
		thisdate=`date +"%d%m.%H"`
		thisdirmon=$mqbackupdir/$thisdate
		if [ ! -d "$thisdirmon" ]; then
			mkdir -p $thisdirmon
		fi
		cd $thisdirmon
		if [ -z "$THISAGENT" ]
		then
		fteListMonitors > list.txt
		else
		fteListMonitors -ma $THISAGENT > list.txt
		fi
		while read line
		do
		AGENT=$(echo $line | cut -d" " -f1)
		MONIT=$(echo $line | cut -d" " -f2)
		echo "getting xml for $MONIT"
		if [ ! -d "${AGENT}" ]; then
			mkdir -p ${AGENT}
			fi
		fteListMonitors -ma $AGENT -mn $MONIT -ox ${AGENT}/${MONIT}.xml
		done < list.txt
	;;
	importmonitors )
	    read -r -p "You copied all xml files in directory:/sourcedir/ENV ? [y/N] " response
		if [[ $response =~ ^([yY][eE][sS]|[yY])$ ]]
		then
		mqbackupdir=$2
		DELAGT=$3
		DELQMGR=$4
		tmpfile="/tmp/listfiles.txt"
		cd $mqbackupdir
		find $mqbackupdir -type f > $tmpfile
		if [ -z "$DELQMGR" ]
		then
		while read thisfile; do 
			fteDeployCM.sh ${thisfile##*/}
			sleep 3
		done < $tmpfile
		else 
		while read thisfile; do 
		    fteDeleteMonitor -ma $DELAGT -mm $DELQMGR -mn $(basename "${thisfile%.xml}")
			sleep 1
			fteDeployCM.sh ${thisfile##*/}
			sleep 3
		done < $tmpfile
		fi
		rm $tmpfile
		else
		echo "OK,nothing happened."
		fi
	;;
	renameagent )
		CONFIGQMGR=$2
		OLD_AGENT_NAME=$3
		NEW_AGENT_NAME=$4
		if [ -z "$NEW_AGENT_NAME" ]
		then
			echo "usage: $0 renameagent Config_Qmanager Old_Agent_name New_Agent_name"
			exit 1
		fi
		cp -R $AGENT_DIR$OLD_AGENT_NAME/ $AGENT_DIR$NEW_AGENT_NAME
		cp -R $AGENT_CERTDIR$OLD_AGENT_NAME/ $AGENT_CERTDIR$NEW_AGENT_NAME
		mv $AGENT_DIR$NEW_AGENT_NAME/${OLD_AGENT_NAME}_create.mqsc $AGENT_DIR$NEW_AGENT_NAME/${NEW_AGENT_NAME}_create.mqsc
		mv $AGENT_DIR$NEW_AGENT_NAME/${OLD_AGENT_NAME}_delete.mqsc $AGENT_DIR$NEW_AGENT_NAME/${NEW_AGENT_NAME}_delete.mqsc
		echo "%s/$OLD_AGENT_NAME/$NEW_AGENT_NAME/g
		w
		q
		" | ex $AGENT_DIR$NEW_AGENT_NAME/${NEW_AGENT_NAME}_create.mqsc
		echo "%s/$OLD_AGENT_NAME/$NEW_AGENT_NAME/g
		w
		q
		" | ex $AGENT_DIR$NEW_AGENT_NAME/${NEW_AGENT_NAME}_delete.mqsc
		echo "%s/$OLD_AGENT_NAME/$NEW_AGENT_NAME/g
		w
		q
		" | ex $AGENT_DIR$NEW_AGENT_NAME/agent.properties
		cat $AGENT_DIR$NEW_AGENT_NAME/agent.properties | grep agentQMg
		cat $AGENT_DIR$NEW_AGENT_NAME/${NEW_AGENT_NAME}_create.mqsc
	;;
	renagentinfiles )
		tmpfile="/tmp/listfiles.txt"
		OLDAGTNAME=$2
		NEWAGTNAME=$3
		mqbackupdir=$4
		if [ -z "$NEWAGTNAME" ]
		then
			echo "usage: $0 renagentinfiles OLD_AGT_NAME NEW_AGT_NAME directory_to_search"
			exit 1
		fi
		find $mqbackupdir -type f > $tmpfile
		while read thisfile; do 
			echo "%s/$OLDAGTNAME/$NEWAGTNAME/g
			w
			q
			" | ex $thisfile
			echo "renaming agent:" $OLDAGTNAME " to: " $NEWAGTNAME " in file: " $thisfile
		done < $tmpfile
		rm $tmpfile
	;;
	* )
	echo "tool for managing FTE monitors and agents"
	echo "usage: $0 deletemonitors AGENT_NAME QMANAGER_NAME APPLICATION(optional)"
	echo "usage: $0 deletetransfers agent Qmanager transfer_uuid"
	echo "usage: $0 exportmonitors AGENT_NAME(optional)"
	echo "usage: $0 importmonitors MONITORS_DIR AGENT(to delete monitors - optional) QMGR(optional)"
	echo "usage: $0 renameagent Config_Qmanager Old_Agent_name New_Agent_name"
	echo "usage: $0 renagentinfiles OLD_AGT_NAME NEW_AGT_NAME directory_to_search"
	esac
