#!/bin/bash
#powered by Vasilev.link
#check expiration of IBM MQ certificates and send email

KDB_FILES="/var/mqm/qmgr/*/ssl/key.kdb"
EXPIRATION_DATE=$(date -d "+7 days" +%s)
CERTS_TO_EXPIRE=()

send_email() {
  boundary="_====_ssl_====_$(date +%Y%m%d%H%M%S)_====_"
  mailfrom=""
  mailto=""
  {
  echo "From: $mailfrom"
  echo "To: $mailto"
  echo "Subject: [WARNING] certificate expiration"
  echo "Content-Type: multipart/mixed; boundary=\"$boundary\""
  echo "MIME-Version: 1.0"
  echo "--$boundary
Content-Type: text/html; charset=UTF8

Dear Team,<br>
The following certificates will expire in 7 days<br>
Please check:<br>
<br>
server: `hostname`<br>
certificates: <br>`
for cert in ${CERTS_TO_EXPIRE[*]}
do
  echo $cert"<br>"
done
`"
echo "--${boundary}--"
  }  | mail $mailto
}


files=($(find $KDB_FILES -type f))
for item in ${files[*]}
do
  CERT_LBL=`runmqakm -cert -list -db ${item} -stashed | grep ibmwebsphere`
  CERT_LBL=ibm${CERT_LBL#*ibm}
  CERT_EXP=`runmqakm -cert -details -db ${item} -label ${CERT_LBL} -stashed | grep "Not After"`
  CERT_EXP=${CERT_EXP/Not After?:/}
  CERT_EXP=`date --date="${CERT_EXP}" +%s`
  if [ $EXPIRATION_DATE -ge $CERT_EXP ];
   then
     CERTS_TO_EXPIRE+=($item)
   fi
done

if (( ${#CERTS_TO_EXPIRE[@]} )); then
    send_email "${CERTS_TO_EXPIRE[*]}"
fi
