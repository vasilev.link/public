lPorts = AdminControl.queryNames('type=ListenerPort,cell=CELLNAME,node=NODENAME,process=server1,*')
import java
lineSeparator = java.lang.System.getProperty('line.separator')
lPortsArray = lPorts.split(lineSeparator)
for lPort in lPortsArray:
  state = AdminControl.getAttribute(lPort, 'started')
  if state == 'false':
    AdminControl.invoke(lPort, 'start')
