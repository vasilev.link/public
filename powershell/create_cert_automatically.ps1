$thisdir="C:\demo\certrequest\"
$import= $thisdir+"listservers.txt"
$TemplateName = "WebServer" # Certtificate template
$CAName = "CA SERVER NAME\CA NAME"
$OpenSSLExe =  "C:\openssl\bin\openssl.exe"
if (!(Test-Path $OpenSSLExe)) {throw "$OpenSSLExe required"}
New-Alias -Name OpenSSL $OpenSSLExe

$RequestTemplate = "[ req ]
default_bits = 2048
default_keyfile = rui.key
distinguished_name = req_distinguished_name
encrypt_key = no
prompt = no
string_mask = nombstr
req_extensions = v3_req
 
[ v3_req ]
keyUsage = digitalSignature, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth
subjectAltName = DNS:FQDN
 
[ req_distinguished_name ]
countryName = # Counry name
stateOrProvinceName = # State
localityName = # State
0.organizationName = # organisation name
organizationalUnitName = # organisation unit name
commonName = FQDN
 
[ req_attributes ]"

ForEach ($server in Get-Content $import){
$path = $thisdir+"share\configs\"+$server+".inf"
$path1 = $thisdir+"share\requests\"+$server+".req"
$logpath = $thisdir+"share\logs\"+$server+".log"
$cername = $thisdir+"share\certs\"+$server+".cer"
$delkey = $thisdir+"share\keys\"+$server+".key_del"
$origkey = $thisdir+"share\keys\"+$server+".key"
$pfxkey = $thisdir+"share\pfx\"+$server+".pfx"

remove-item $path1,$cername,$path,$delkey,$origkey

$Out = ($RequestTemplate -replace "FQDN", $server) | Out-File $path -Encoding Default -Force
OpenSSL req -new -nodes -out $path1 -days 730 -keyout $delkey -config $path
OpenSSL rsa -in $delkey -out $origkey

certreq -submit -attrib "CertificateTemplate:WebServer" -config $CAName $path1 $cername |out-file $logpath

if (!(Test-Path $cername)) {throw "No cert.."}

$certid = Get-content $logpath |Select-String -Pattern 'Requestid: "' 
$certid = $certid -replace 'Requestid: "' -replace '"'

certreq -retrieve -f -config $CAName $certid $cername
OpenSSL pkcs12 -export -out $pfxkey -inkey $origkey -in $cername -password pass:123
}
exit
