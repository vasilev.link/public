strBody = "text in the body"
strTO = "email@example.com"

Set fso = CreateObject("Scripting.FileSystemObject")
Set objEmail = CreateObject("CDO.Message")

objEmail.From = "fromemail@example.com"
objEmail.To = strTO
objEmail.CC = "" 
objEmail.Subject = "text in the subject"
objEmail.Textbody = strBody

'objEmail.AddAttachment "link to the file for attachment"


objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "SMTP server DNS name" 
objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
''If needed authentication!!
'objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
'objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") = "username"
'objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") = "password"
'objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = False

objEmail.Configuration.Fields.Update

objEmail.Send
